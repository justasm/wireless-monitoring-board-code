// assorted links
// http://forum.arduino.cc/index.php?topic=74977.0
// http://www.fiz-ix.com/2012/01/how-to-configure-arduino-timer-2-registers-to-drive-an-ultrasonic-transducer-with-a-square-wave/
// http://www.righto.com/2009/07/secrets-of-arduino-pwm.html
// good reference http://www.protostack.com/blog/2010/09/timer-interrupts-on-an-atmega168/

// uses code and comments from http://forum.arduino.cc/index.php?topic=74977.0

// This program uses the 'Toggle OC2A on Compare Match' mode along with the 'Clear Timer on Compare Match (CTC)' mode 
//   to generate a square wave.  The disadvantage of this technique is that the output pin is fixed by the hardware.  
//   The advantage is that this technique does not use any interrupts and does use any code (or processor time) once
//   it is set up.  It does tie up a timer (timer 2 in this case).

// The output frequency is determined by the clock frequency, the prescale factor, and the value loaded into OCR2A.
// The output frequency = (clock frequency / (2 * prescale factor * (1 + OCR2A)))
// The Arduino clock frequency is fixed at 16 MHz so the factors we have to work with are the prescale factor, and 
// the value loaded into OCR2A.

// For any given prescale factor the maximum output frequency will occur when OCR2A is loaded with 00.
// For any given prescale factor the minimum output frequency will occur when OCR2A is loaded with 255.
//   For a prescale factor of 1, fmax = 8 MHz and fmin = 31250 Hz
//   For a prescale factor of 8, fmax = 1 MHz and fmin = 3906.3 Hz
//   For a prescale factor of 32, fmax = 250 KHz and fmin = 976.6 Hz

// Reworking the equation gives: OCR2A = (clock frequency / (output frequency * 2 * prescale factor)) -1


void setup() {
   // Configuration:
  
  // COMxx - 'Compare Output Mode' is set to 'Toggle OC2A on Compare Match'
  // set COMxx = 1 (B01) for COMA and/or COMB depending on which Arduino pin we want to target
  // ex: TCCR = B0101... for both A and B output pins in the above mode
  
  // WGM - 'Waveform Generation Mode' is set to 2 (B010) for 'Clear Timer on Compare Match (CTC)'
  // bits are split among both TCCR registers
  
  // FOC is not used 
  
  // Prescaler - CSxx is set to 1 (B001) for x1 prescaling, keeping clock at 16MHz
  
  // Timer layout (Timer2)
  // TCCR2A - [COM2A1, COM2A0, COM2B1, COM2B0, reserved, reserved, WGM21, WGM20]
  // TCCR2B - [FOC2A, FOC2B, reserved, reserved, WGM22, CS22, CS21, CS20]
  
  // Timer 0 for pins 6 (A) and 5 (B)
  TCCR0A = B01010010;
  TCCR0B = B00000001;

  // to obtain ~ 4Mhz -> OCR0A = 2-1 = 1 (2 clock cycles per toggle, divides clock by 4 in total)
  OCR0A = 1;
  
  // Set up one or more output pin
  // IMPORTANT: needs to corerspond to output pin (A or B) of timer of course
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

}
