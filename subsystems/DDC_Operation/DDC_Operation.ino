// DDC114 Test
// Attempting to configure, run the device, and output data.
// Written for Arduino Pro Mini 3.3V/8MHz.

const int PIN_RANGE0 = 7;
const int PIN_RANGE1 = 8;
const int PIN_RANGE2 = 9;

const int PIN_FORMAT = 6;
const int PIN_CLK = 5;
const int PIN_CONV = 4;
const int PIN_TEST = 14;
const int PIN_RESETn = 15;

const int PIN_DVALIDn = 3;

// part of SPI
const int PIN_DCLK = 13; // SCLK
const int PIN_DOUT = 12; // MISO

void initSpi(){
  SPI.begin();
  SPI.setDataMode(SPI_MODE????);
  SPI.setBitOrder(MSBFIRST??);
  SPI.setClockDivider(SPI_CLOCK_DIV2); // 4MHz DCLK
}

void initPins(){
  pinMode(PIN_RANGE0, OUTPUT);
  pinMode(PIN_RANGE1, OUTPUT);
  pinMode(PIN_RANGE2, OUTPUT);
  
  pinMode(PIN_FORMAT, OUTPUT);
  pinMode(PIN_CLK, OUTPUT);
  pinMode(PIN_CONV, OUTPUT);
  pinMode(PIN_TEST, OUTPUT);
  pinMode(PIN_RESETn, OUTPUT);
  
  pinMode(PIN_DVALIDn, INPUT);
  
  // configure with defaults
  digitalWrite(PIN_RESETn, HIGH);
  digitalWrite(PIN_TEST, LOW);
  // TODO FORMAT??
  digitalWrite(PIN_FORMAT, LOW);
  // TODO RANGE??
  digitalWrite(PIN_RANGE0, LOW);
  digitalWrite(PIN_RANGE1, LOW);
  digitalWrite(PIN_RANGE2, LOW);
  
  // TODO set up clocks??
  digitalWrite(PIN_CLK, LOW);
  digitalWrite(PIN_CONV, LOW);
}

void setup() {
  Serial.begin(9600);
  
  initSpi();
  initPins();
  
  Serial.println("- - - - - - - - - -");
  Serial.println("Starting DDC test.");
  Serial.println("- - - - - - - - - -");
  delay(100);
  
  // TODO EVERYTHING :D
}

void loop() {
  // put your main code here, to run repeatedly:

}
