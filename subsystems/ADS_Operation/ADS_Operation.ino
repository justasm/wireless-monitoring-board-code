// ADS1298 Test
// Attempting to write/read registers as well as output data.

/*
NOTE on CLK
------------------------------
ADS CLK is 2.048 MHz
op decoding time = 4 * tCLK = 1.96 us
if SCLK = 4MHz (default - setClockDivider(21)), one byte is transferred in 2 us
THEREFORE no artificial delay required between subsequent bytes
(although we currently have a healthy amount of delayMicroseconds() sprinkled)

RESET takes 18 * tCLK = 8.82 us

*/

#include <SPI.h>
#include "Mode.h"
#include "ADS1298.h"

// SPI control pins
const int PIN_DRDY = 35;
const int PIN_SS = 52;

const Mode MODE = RDATAC_INTERRUPT; // RDATAC occasionally produces faulty reads..

volatile boolean reading;
//volatile boolean dataAvailable;
volatile uint8_t latestData[27];
volatile int dataBytesRead = 0;

const int CHANNEL_UNDER_TEST = 2;
const uint8_t CUT_GAIN = GAIN_6;
const boolean CUT_TEST = false;

void initSPI(){
  SPI.begin();
  SPI.setDataMode(SPI_MODE1); // CPOL = 0, CPHA = 1, see datasheet pg. 15
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(PIN_SS, 168); // Due: 21 - 4MHz, 84 - 1MHz
//  SPI.setClockDivider(SPI_CLOCK_DIV4); // Non-Due 16MHz boards: SPI_CLOCK_DIV4 - 4MHz
}

void initPins(){
  pinMode(PIN_DRDY, INPUT);
  pinMode(PIN_SS, OUTPUT);
  digitalWrite(PIN_SS, HIGH);
}

void setup() {
  Serial.begin(9600);
  
  initSPI();
  initPins();
  
  Serial.println("- - - - - - - - - -");
  Serial.println("Starting ADS test.");
  Serial.println("- - - - - - - - - -");
  delay(100);
  
  // ADS startup
  // see datasheet pg. 48
  performOpVerbose(OP_RESET, "RESET");
  delayMicroseconds(25);
  performOpVerbose(OP_SDATAC, "SDATAC");
  delayMicroseconds(2);
//  performOpVerbose(OP_STOP, "STOP"); // is this necessary??.. doesn't seem like it
  delayMicroseconds(2);
  uint8_t id = readRegVerbose(REG_ID); // returns a constant ID, must be B10010010 for ADS1298
  Serial.print("ID "); Serial.println(id == ADS_1298_ID? "MATCHES" : "DOES NOT MATCH");
  Serial.println("- - - - - - - - - -");
  delayMicroseconds(2);
  
//  writeRegVerbose(REG_CONFIG2, B00010000); // generate test signal internally - done by setChannelOn()

  // set all GPIO pins to output 0 (floating CMOS inputs can flicker on and off, creating noise)
  writeRegVerbose(REG_GPIO, 0);

  // enable low-power mode, multiple readback mode, default output data rate
  writeRegVerbose(REG_CONFIG1, CONFIG1_BASE | CONFIG1_MULTIPLE_READBACK | CONFIG1_LOW_POWER_250SPS);
  
  // enable internal reference buffer!!!
  writeRegVerbose(REG_CONFIG3, CONFIG3_BASE | CONFIG3_ENABLE_REF_BUF);
  delay(150); // internal reference start-up time

  // power down most channels
  for(int i = 1; i <= 8; i++){
    setChannelOnVerbose(i, i == CHANNEL_UNDER_TEST, CUT_GAIN, CUT_TEST);
  }
  
  readRegVerbose(REG_CONFIG1);
  readRegVerbose(REG_CONFIG2);
  readRegVerbose(REG_CONFIG3);
  readRegVerbose(REG_LOFF);
  for(int i = 1; i <= 8; i++){
    readRegVerbose(REG_LOFF + i);
  }
  readRegVerbose(REG_GPIO);
  
  performOpVerbose(OP_START, "START");
  delayMicroseconds(2);
  
  reading = false;
//  dataAvailable = false;
  
  switch(MODE){
    // --- OPTION 1 ---
    // RDATAC - ech, finally works, too!
    case RDATAC:
      performOpVerbose(OP_RDATAC, "RDATAC");
      break;
    case RDATAC_INTERRUPT:
      performOpVerbose(OP_RDATAC, "RDATAC");
      delayMicroseconds(2);
      attachInterrupt(PIN_DRDY, onDataReadyRDATAC, FALLING);
      break;
      
    // --- OPTION 2 ---
    // RDATA - works!
    case RDATA_INTERRUPT:
        attachInterrupt(PIN_DRDY, onDataReadyRDATA, FALLING);
        // MEGA
//        attachInterrupt(0, onDataReadyRDATA, FALLING); // HARDWIRED FOR MEGA PIN 2
      break;
    default:
      break;
  }
  
  Serial.println("- - - - - - - - - -");
}

void loop() {
  // put your main code here, to run repeatedly:
  if(MODE == RDATA || MODE == RDATAC){
    if(digitalRead(PIN_DRDY) == LOW){
      switch(MODE){
        case RDATA: onDataReadyRDATA(); break;
        case RDATAC: onDataReadyRDATAC(); break;
        default: break;
      }
    }
  }
  
  if(MODE == RDATAC || MODE == RDATAC_INTERRUPT){
//    Serial.print(dataBytesRead); Serial.println(" bytes read since last print");
    dataBytesRead = 0;
    
    noInterrupts();
//    printData(latestData);
    printDouble(getVoltageFromLong(fromDataGetChannel(latestData, CHANNEL_UNDER_TEST)), 6);
    interrupts();
    Serial.println("");
  }

  // this approach does not work for whatever reason..
//  if(dataAvailable){
//    noInterrupts();
//    digitalWrite(PIN_SS, LOW);
//    readDataVerbose();
//    digitalWrite(PIN_SS, HIGH);
//    dataAvailable = false;
//    interrupts();
//  }
}

// continuous data retrieval
void onDataReadyRDATAC(){
  if(reading) return;
  if(dataBytesRead > 0) return;
  reading = true;
  digitalWrite(PIN_SS, LOW);

  readData(latestData);

  digitalWrite(PIN_SS, HIGH);
  reading = false;
}

// data-on-demand
void onDataReadyRDATA(){
  if(reading) return;
  reading = true;
  digitalWrite(PIN_SS, LOW);
  
//  Serial.print("Op RDATA 0x");
//  Serial.println(OP_RDATA, HEX);
  
  SPI.transfer(OP_RDATA);
  delayMicroseconds(1);
  
//  readDataVerbose();
  readDataPrintChannelDouble(CHANNEL_UNDER_TEST);
//  readDataVerbose();
  
  digitalWrite(PIN_SS, HIGH);
  reading = false;
}

// reads all 27 bytes of status and channel output data
// stores them in the provided array
void readData(volatile uint8_t *dataOut){
  for(int i = 0; i < 27; i++){
    dataOut[i] = SPI.transfer(0x00);
    dataBytesRead++;
  }
}

// reads all status and channel output data
// must only be called when this data is available on the SPI line, CS is low, etc.
// reads 24 status bits + 24 bits / channel
// see datasheet pg. 45-46
// works for anything BUT 32- and 64-kSPS data rates (which only contain 16 bits / channel)
void readDataVerbose(){
  Serial.print("Data: ");
  for(int i = 0; i < 27; i++){
    uint8_t out = SPI.transfer(0x00);
    Serial.print(out, HEX);
    Serial.print(" ");
    if((i+1)%3 == 0) Serial.print("| ");
  }
  Serial.println(" ");
}

uint8_t chBytes[3];
// does same as readDataVerbose() but only prints the data for the given channel
void readDataPrintChannel(int channel){
  for(int i = 0; i < 9; i++){
      for(int j = 0; j < 3; j++){
        chBytes[j] = SPI.transfer(0x00);
      }
      if(i != channel) continue;
      Serial.print(signed24BitsToLong(chBytes));
    }
  Serial.println("");
}

void readDataPrintChannelDouble(int channel){
  for(int i = 0; i < 9; i++){
      for(int j = 0; j < 3; j++){
        chBytes[j] = SPI.transfer(0x00);
      }
      if(i != channel) continue;
      printDouble(getVoltageFromLong(signed24BitsToLong(chBytes)), 6);
    }
  Serial.println("");
}

void printData(volatile uint8_t *data){
  Serial.print("Data: ");
  for(int i = 0; i < 27; i++){
    Serial.print(data[i], HEX);
    Serial.print(" ");
    if((i+1)%3 == 0) Serial.print("| ");
  }
  Serial.println(" ");
}

long fromDataGetChannel(volatile uint8_t *data, int channel){
  for(int j = 0; j < 3; j++){
    chBytes[j] = data[channel * 3 + j];
  }
  return signed24BitsToLong(chBytes);
}

double getVoltageFromLong(long value){
  return (double)value * 2.4f / 8388607L;
}

long signed24BitsToLong(uint8_t* bytes){
  long temp = (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8);
  return temp / (1 << 8);
}

long signed24BitsToLong(uint8_t msb, uint8_t b, uint8_t lsb){
  long temp = (msb << 24) | (b << 16) | (lsb << 8);
  return temp / (1 << 8);
}

// prints val with number of decimal places determine by precision
// precision is a number from 0 to 6 indicating the desired decimial places
// example: lcdPrintDouble( 3.1415, 2); // prints 3.14 (two decimal places)
void printDouble(double val, byte precision){
  if(val < 0.0){
    Serial.print('-');
    val = -val;
  }
  
  Serial.print (int(val));  //prints the int part
  if(precision > 0) {
    Serial.print("."); // print the decimal point
    unsigned long frac;
    unsigned long mult = 1;
    byte padding = precision - 1;
    while(precision--) mult *= 10;
     
    if(val >= 0)
      frac = (val - int(val)) * mult;
    else
      frac = (int(val)- val) * mult;
    unsigned long frac1 = frac;
    while(frac1 /= 10) padding--;
    while(padding--) Serial.print("0");
    Serial.print(frac,DEC);
  }
}

// use for the most basic setup, w/ no configuration of registers
void setupMinimal(){
  Serial.begin(9600);
  
  initSPI();
  initPins();
  
  performOpVerbose(OP_RESET, "RESET");
  delayMicroseconds(25);
  performOpVerbose(OP_SDATAC, "SDATAC");
  delayMicroseconds(2);
}
