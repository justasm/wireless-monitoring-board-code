/*
ADS1298 Operation codes and register configuration constants.
*/

// what value of REG_ID should be
const int ADS_1298_ID = B10010010;

// opcodes
const int OP_WAKEUP  = 0x02;
const int OP_STANDBY = 0x04;
const int OP_RESET   = 0x06;
const int OP_START   = 0x08;
const int OP_STOP    = 0x0a;

const int OP_RDATAC  = 0x10;
const int OP_SDATAC  = 0x11;
const int OP_RDATA   = 0x12;

const int OP_RREG    = 0x20;
const int OP_WREG    = 0x40;

// register addresses
const int REG_ID      = 0x00;
const int REG_CONFIG1 = 0x01;
const int REG_CONFIG2 = 0x02;
const int REG_CONFIG3 = 0x03;
const int REG_LOFF    = 0x04;

const int REG_GPIO    = 0x14;

// a selection of CONFIG1 settings
const uint8_t CONFIG1_BASE              = 0;
const uint8_t CONFIG1_MULTIPLE_READBACK = 1 << 6; // vs DAISY_CHAIN default
const uint8_t CONFIG1_DATARATE_DEFAULT  = B00000110;
const uint8_t CONFIG1_LOW_POWER_16kSPS  = B00000000;
const uint8_t CONFIG1_HIGH_RES_32kSPS   = B10000000; // beware - 32kSPS returns only 16 bits of data / channel
const uint8_t CONFIG1_LOW_POWER_8kSPS   = B00000001;
const uint8_t CONFIG1_HIGH_RES_16kSPS   = B10000001;
const uint8_t CONFIG1_LOW_POWER_4kSPS   = B00000010;
const uint8_t CONFIG1_HIGH_RES_8kSPS    = B10000010;
const uint8_t CONFIG1_LOW_POWER_2kSPS   = B00000011;
const uint8_t CONFIG1_HIGH_RES_4kSPS    = B10000011;
const uint8_t CONFIG1_LOW_POWER_1kSPS   = B00000100;
const uint8_t CONFIG1_HIGH_RES_2kSPS    = B10000100;
const uint8_t CONFIG1_LOW_POWER_500SPS  = B00000101;
const uint8_t CONFIG1_HIGH_RES_1kSPS    = B10000101;
const uint8_t CONFIG1_LOW_POWER_250SPS  = B00000110;
const uint8_t CONFIG1_HIGH_RES_500SPS   = B10000110;
const uint8_t CONFIG1_DEFAULT           = B00000110;

// a selection of CONFIG3 settings
const uint8_t CONFIG3_BASE              = 1 << 6;
const uint8_t CONFIG3_ENABLE_REF_BUF    = 1 << 7; // vs power-down internal reference buffer

// channel gain settings
const uint8_t GAIN_6      = B000;
const uint8_t GAIN_1      = B001;
const uint8_t GAIN_2      = B010;
const uint8_t GAIN_3      = B011;
const uint8_t GAIN_4      = B100;
const uint8_t GAIN_8      = B101;
const uint8_t GAIN_12     = B110;
