// Register input / output operations.
// Verbose versions with debug logging below.

void performOp(uint8_t op){
  digitalWrite(PIN_SS, LOW);
  
  SPI.transfer(op);
  delayMicroseconds(1);
  
  digitalWrite(PIN_SS, HIGH);
}

void writeReg(int addr, uint8_t b){
  digitalWrite(PIN_SS, LOW);
  
  SPI.transfer(OP_WREG | addr); // WREG = 0x40
  delayMicroseconds(2);
  SPI.transfer(0x00); // # regs to write - 1
  delayMicroseconds(2);
  SPI.transfer(b);
  delayMicroseconds(1);
  
  digitalWrite(PIN_SS, HIGH);
}

uint8_t readReg(int addr){
  digitalWrite(PIN_SS, LOW);
  
  SPI.transfer(OP_RREG | addr); // RREG = 0x20
  delayMicroseconds(2);
  SPI.transfer(0x00); // # regs to read - 1
  delayMicroseconds(2);
  
  uint8_t response = SPI.transfer(0x00);
  delayMicroseconds(1);  
  
  digitalWrite(PIN_SS, HIGH);
  return response;
}

// writes to CHnSET registers
// channel must be 1-8
void setChannelOn(int channel, boolean on, int gain, boolean test){
  if(channel < 1 || channel > 8) return;
  // first channel settings register is at 0x05, last at 0x0C
  int addr = channel + 0x04;
  
  uint8_t val = on ? B00000000 : B10000001; // see ADS datasheet pg 28
  
  // configure gain
  val = val | (gain << 4);
  
  if(test){
    // set TEST signal to be generated internally, see pg 25
    writeReg(REG_CONFIG2, B00010000);
    delayMicroseconds(2);
    val = val | B00000101; // test signal mask
  }
  writeReg(addr, val);
}

/*
VERBOSE VERSIONS (for easier debugging)
-------------------------------------------------------------------------
*/

// pass in operation name to log in a readable manner
void performOpVerbose(uint8_t op, char* opName){
  performOp(op);
  
  Serial.print("Op ");
  Serial.print(opName);
  Serial.print(" 0x");
  Serial.println(op, HEX);
}

void writeRegVerbose(int addr, uint8_t b){
  Serial.print("Wrote ");
  Serial.print(b, BIN); // change to HEX or DEC as desired
  Serial.print(" to register 0x");
  Serial.println(addr, HEX);
  
  writeReg(addr, b);
}

uint8_t readRegVerbose(int addr){
  uint8_t b = readReg(addr);
  
  Serial.print("Read ");
  Serial.print(b, BIN); // change to HEX or DEC as desired
  Serial.print(" from register 0x");
  Serial.println(addr, HEX);
  
  return b;
}

void setChannelOnVerbose(int channel, boolean on, int gain, boolean test){
  setChannelOn(channel, on, gain, test);
  
  Serial.print("Channel ");
  Serial.print(channel);
  Serial.print(" turned ");
  Serial.print(on ? "on" : "off");
  Serial.print(" gain ");
  switch(gain){
    case GAIN_1: Serial.print("1"); break;
    case GAIN_2: Serial.print("2"); break;
    case GAIN_3: Serial.print("3"); break;
    case GAIN_4: Serial.print("4"); break;
    case GAIN_6: Serial.print("6"); break;
    case GAIN_8: Serial.print("8"); break;
    case GAIN_12: Serial.print("12"); break;
    default: Serial.print("INVALID"); break;
  }
  Serial.print(" test ");
  Serial.println(test ? "on" : "off");
}
