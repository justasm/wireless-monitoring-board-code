#include <XBee.h>

const long BAUD_RATE = 57600;
const int PAYLOAD_SIZE = 25;
const int TEST_PACKET_COUNT = 300;
const int INTER_PACKET_DELAY_MS = 100;
XBee transmitter = XBee();

uint8_t payload[PAYLOAD_SIZE];
uint8_t packetId;

// send to coordinator, 16-bit address = 0, with default options (ACK)
Tx16Request txRequest = Tx16Request(0x0000, 0, payload, sizeof(payload), 1);

int packetCount;

void setup() {
  Serial.begin(BAUD_RATE);
  transmitter.setSerial(Serial);
  
  for(int i = 0; i < sizeof(payload); i++){
    payload[i] = 0x10 + i; // demo byte
  }
  packetId = 1;
  packetCount = 1;
}

void loop() {
  if(packetCount > TEST_PACKET_COUNT) return;
  delay(INTER_PACKET_DELAY_MS);
  
  payload[0] = packetId;
  transmitter.send(txRequest);
  packetId++;
  packetCount++;
}
