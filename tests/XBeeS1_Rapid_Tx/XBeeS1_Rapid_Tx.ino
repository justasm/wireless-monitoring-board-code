#include <XBee.h>

XBee xbee = XBee();

uint8_t payload[27];

// send to coordinator, 16-bit address = 0, with ACK
Tx16Request tx = Tx16Request(0x0000, ACK_OPTION, payload, sizeof(payload), 1);

TxStatusResponse txStatus = TxStatusResponse();

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  xbee.setSerial(Serial1);
  
  for(int i = 0; i < sizeof(payload); i++){
    payload[i] = 0xA3; // demo byte
  }
}

void loop() {
  tx.setFrameId(xbee.getNextFrameId()); // increment frame id
  payload[26] = tx.getFrameId();
  xbee.send(tx);
  Serial.print("TX REQUEST "); Serial.println(tx.getFrameId());
  
  // enable to slow down transmission, but decode ACKs
//  xbee.readPacket();
//  if(xbee.getResponse().isAvailable()){
//    if(xbee.getResponse().getApiId() == TX_STATUS_RESPONSE){
//      xbee.getResponse().getZBTxStatusResponse(txStatus);
//      
//      Serial.print(txStatus.getFrameId()); Serial.print(" TX STATUS ");
//      if(txStatus.getStatus() == SUCCESS){
//        Serial.println("SUCCESS");
//      } else {
//        Serial.println("FAILURE");
//      }
//    }
//  } else if(xbee.getResponse().isError()){
//    Serial.println("ERROR");
//  } else {
//    Serial.println("NO STATUS");
//  }
  
  Serial.print("Time: "); Serial.println(millis());
}
