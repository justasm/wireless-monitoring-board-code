#include <XBee.h>

const long BAUD_RATE = 57600;
const int MAX_PAYLOAD_SIZE_BYTES = 25;
XBee bouncer = XBee();

uint8_t payload[MAX_PAYLOAD_SIZE_BYTES];

// send to coordinator, 16-bit address = 0, with default options (ACK)
Tx16Request txRequest = Tx16Request(0x0000, 0, payload, sizeof(payload), 1);

Rx16Response rx16Response = Rx16Response();
int responseLength;
int i;

void setup() {
  Serial.begin(BAUD_RATE);
  bouncer.setSerial(Serial);
}

void loop() {
  bouncer.readPacket();
  
  if(bouncer.getResponse().isAvailable()){
    // got something
    if(bouncer.getResponse().getApiId() == RX_16_RESPONSE){
      bouncer.getResponse().getRx16Response(rx16Response);
      
      responseLength = rx16Response.getDataLength();
      for(i = 0; i < responseLength; i++){
        payload[i] = rx16Response.getData()[i];
      }
      
      txRequest.setPayloadLength(responseLength);
      
      bouncer.send(txRequest);
    }
  }
}
