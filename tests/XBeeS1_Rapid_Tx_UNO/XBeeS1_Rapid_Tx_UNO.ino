#include <XBee.h>

const int TEST_DURATION_MS = 10000;
XBee xbee = XBee();

uint8_t payload[25];
unsigned long startTime;
uint8_t frameId;

// send to coordinator, 16-bit address = 0, with default options (ACK)
Tx16Request tx = Tx16Request(0x0000, 0, payload, sizeof(payload), 1);

void setup() {
  Serial.begin(57600);
  xbee.setSerial(Serial);
  
  for(int i = 0; i < sizeof(payload); i++){
    payload[i] = 0x10 + i; // demo byte
  }
  frameId = 1;
  
  startTime = millis();
}

void loop() {
  if(millis() - startTime > TEST_DURATION_MS) return;
  
//  frameId = xbee.getNextFrameId();
  tx.setFrameId(frameId); // set & increment frame id
  payload[0] = frameId;
  xbee.send(tx);
  frameId++;
}
