#include <XBee.h>

const long BAUD_RATE = 57600;
const int PAYLOAD_SIZE = 25;
const int TEST_DURATION_MS = 10000;
XBee transmitter = XBee();

uint8_t payload[PAYLOAD_SIZE];
unsigned long startTime;
uint8_t frameId;

// send to coordinator, 16-bit address = 0, with default options (ACK)
Tx16Request txRequest = Tx16Request(0x0000, 0, payload, sizeof(payload), 1);

void setup() {
  Serial.begin(BAUD_RATE);
  transmitter.setSerial(Serial);
  
  for(int i = 0; i < sizeof(payload); i++){
    payload[i] = 0x10 + i; // demo byte
  }
  frameId = 1;
  
  startTime = millis();
}

void loop() {
  if(millis() - startTime > TEST_DURATION_MS) return;
  
  txRequest.setFrameId(frameId);
  payload[0] = frameId;
  transmitter.send(txRequest);
  frameId++;
}
