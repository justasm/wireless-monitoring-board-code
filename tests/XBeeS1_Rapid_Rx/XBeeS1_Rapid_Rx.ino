
#include<XBee.h>

XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
Rx16Response rx16 = Rx16Response();

uint8_t data[27];

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  
  xbee.setSerial(Serial1);
}

void loop() {
  xbee.readPacket();
  
  if(xbee.getResponse().isAvailable()){
    Serial.println("RECEIVED PACKET");
    if(xbee.getResponse().getApiId() == RX_16_RESPONSE){
      xbee.getResponse().getRx16Response(rx16);
//      data = rx16.getData();
      
      for(int i = 0; i < 27; i++){
        Serial.print(rx16.getData(i));
        Serial.print(" ");
      }
      Serial.println("");
//        Serial.println(rx16.getData(26));
    }
  } else if(xbee.getResponse().isError()){
    Serial.print("ERROR "); Serial.println(xbee.getResponse().getErrorCode());
  }
  
  Serial.print("Time: "); Serial.println(millis());
}
